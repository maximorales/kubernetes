***Pasos para despliegue de la aplicación 'webapp'.***


1- Crear volumen persistente utilizando el fichero 'webapp-pv.yaml'.

$ kubectl apply -f webapp-pv.yaml


2- Crear el reclamo de espacio de almacenamiento en el cluster con el fichero 'webapp-pvc.yaml'.

$ kubectl apply -f webapp-pvc.yaml


3- Ejecutar el deployment de pods utilizando el fichero 'webapp-deployment.yaml'.

$ kubectl apply -f webapp-deployment.yaml


4- Verificar la ejecución del deployment.

$ kubectl get deployment

En información que brinda la salida del comando deberá verse un deployment en ejecución con el nombre 'webapp-deployment'.


5- Verificar la existencia de los pods del deployment.

$ kubectl get pods

En la información que brinda la salida del comando deberán visualizarse cuatro réplicas de pod generadas por el deployment.


6- Copiar el contenido de la carpeta 'pagina' al directorio de almacenamiento permanente en local host.

$ cp -r pagina/* /mnt/webapp-pv


7- Ejecutar el siguiente comando de creación de proxy para acceso externo a la aplicación.

$ kubectl port-forward deployment/webapp-deployment 8080:80 --address 0.0.0.0


8- Acceder a la aplicación desde un navegador.

http://<host_ip_address>:8080
